﻿namespace WinMDS
{
    partial class WinMDS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.buttonViewCustomer = new System.Windows.Forms.Button();
            this.textBox_firstName = new System.Windows.Forms.TextBox();
            this.textBox_lastName = new System.Windows.Forms.TextBox();
            this.textBox_email = new System.Windows.Forms.TextBox();
            this.textBox_mobile = new System.Windows.Forms.TextBox();
            this.FirstName = new System.Windows.Forms.Label();
            this.LastName = new System.Windows.Forms.Label();
            this.Email = new System.Windows.Forms.Label();
            this.Mobile = new System.Windows.Forms.Label();
            this.button_AddCustomer = new System.Windows.Forms.Button();
            this.button_DeleteCustomer = new System.Windows.Forms.Button();
            this.button_UpdateCustomer = new System.Windows.Forms.Button();
            this.label_CusID = new System.Windows.Forms.Label();
            this.textBox_CusID = new System.Windows.Forms.TextBox();
            this.button_Clear = new System.Windows.Forms.Button();
            this.label_Notofication = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.GridColor = System.Drawing.Color.Gray;
            this.dataGridView1.Location = new System.Drawing.Point(247, 220);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(540, 188);
            this.dataGridView1.TabIndex = 0;
            // 
            // buttonViewCustomer
            // 
            this.buttonViewCustomer.Location = new System.Drawing.Point(60, 67);
            this.buttonViewCustomer.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.buttonViewCustomer.Name = "buttonViewCustomer";
            this.buttonViewCustomer.Size = new System.Drawing.Size(112, 36);
            this.buttonViewCustomer.TabIndex = 1;
            this.buttonViewCustomer.Text = "View Customer";
            this.buttonViewCustomer.UseVisualStyleBackColor = true;
            this.buttonViewCustomer.Click += new System.EventHandler(this.buttonViewCustomer_Click);
            // 
            // textBox_firstName
            // 
            this.textBox_firstName.Location = new System.Drawing.Point(355, 58);
            this.textBox_firstName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox_firstName.Name = "textBox_firstName";
            this.textBox_firstName.Size = new System.Drawing.Size(380, 20);
            this.textBox_firstName.TabIndex = 3;
            // 
            // textBox_lastName
            // 
            this.textBox_lastName.Location = new System.Drawing.Point(355, 92);
            this.textBox_lastName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox_lastName.Name = "textBox_lastName";
            this.textBox_lastName.Size = new System.Drawing.Size(380, 20);
            this.textBox_lastName.TabIndex = 4;
            // 
            // textBox_email
            // 
            this.textBox_email.Location = new System.Drawing.Point(355, 124);
            this.textBox_email.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox_email.Name = "textBox_email";
            this.textBox_email.Size = new System.Drawing.Size(380, 20);
            this.textBox_email.TabIndex = 5;
            // 
            // textBox_mobile
            // 
            this.textBox_mobile.Location = new System.Drawing.Point(355, 162);
            this.textBox_mobile.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox_mobile.Name = "textBox_mobile";
            this.textBox_mobile.Size = new System.Drawing.Size(380, 20);
            this.textBox_mobile.TabIndex = 6;
            // 
            // FirstName
            // 
            this.FirstName.AutoSize = true;
            this.FirstName.Location = new System.Drawing.Point(244, 62);
            this.FirstName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.FirstName.Name = "FirstName";
            this.FirstName.Size = new System.Drawing.Size(70, 15);
            this.FirstName.TabIndex = 7;
            this.FirstName.Text = "FirstName :";
            // 
            // LastName
            // 
            this.LastName.AutoSize = true;
            this.LastName.Location = new System.Drawing.Point(244, 94);
            this.LastName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LastName.Name = "LastName";
            this.LastName.Size = new System.Drawing.Size(70, 15);
            this.LastName.TabIndex = 8;
            this.LastName.Text = "LastName :";
            // 
            // Email
            // 
            this.Email.AutoSize = true;
            this.Email.Location = new System.Drawing.Point(244, 127);
            this.Email.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Email.Name = "Email";
            this.Email.Size = new System.Drawing.Size(45, 15);
            this.Email.TabIndex = 9;
            this.Email.Text = "Email :";
            // 
            // Mobile
            // 
            this.Mobile.AutoSize = true;
            this.Mobile.Location = new System.Drawing.Point(244, 164);
            this.Mobile.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Mobile.Name = "Mobile";
            this.Mobile.Size = new System.Drawing.Size(99, 15);
            this.Mobile.TabIndex = 10;
            this.Mobile.Text = "Mobile Number :";
            // 
            // button_AddCustomer
            // 
            this.button_AddCustomer.Location = new System.Drawing.Point(60, 129);
            this.button_AddCustomer.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button_AddCustomer.Name = "button_AddCustomer";
            this.button_AddCustomer.Size = new System.Drawing.Size(112, 37);
            this.button_AddCustomer.TabIndex = 11;
            this.button_AddCustomer.Text = "Add Customer";
            this.button_AddCustomer.UseVisualStyleBackColor = true;
            this.button_AddCustomer.Click += new System.EventHandler(this.button_AddCustomer_Click);
            // 
            // button_DeleteCustomer
            // 
            this.button_DeleteCustomer.Location = new System.Drawing.Point(60, 193);
            this.button_DeleteCustomer.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button_DeleteCustomer.Name = "button_DeleteCustomer";
            this.button_DeleteCustomer.Size = new System.Drawing.Size(112, 38);
            this.button_DeleteCustomer.TabIndex = 12;
            this.button_DeleteCustomer.Text = "Delete Customer";
            this.button_DeleteCustomer.UseVisualStyleBackColor = true;
            this.button_DeleteCustomer.Click += new System.EventHandler(this.button_DeleteCustomer_Click);
            // 
            // button_UpdateCustomer
            // 
            this.button_UpdateCustomer.Location = new System.Drawing.Point(60, 255);
            this.button_UpdateCustomer.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button_UpdateCustomer.Name = "button_UpdateCustomer";
            this.button_UpdateCustomer.Size = new System.Drawing.Size(112, 37);
            this.button_UpdateCustomer.TabIndex = 13;
            this.button_UpdateCustomer.Text = "Update";
            this.button_UpdateCustomer.UseVisualStyleBackColor = true;
            this.button_UpdateCustomer.Click += new System.EventHandler(this.button_UpdateCustomer_Click);
            // 
            // label_CusID
            // 
            this.label_CusID.AutoSize = true;
            this.label_CusID.Location = new System.Drawing.Point(244, 28);
            this.label_CusID.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_CusID.Name = "label_CusID";
            this.label_CusID.Size = new System.Drawing.Size(81, 15);
            this.label_CusID.TabIndex = 14;
            this.label_CusID.Text = "Customer ID :";
            // 
            // textBox_CusID
            // 
            this.textBox_CusID.Location = new System.Drawing.Point(355, 24);
            this.textBox_CusID.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.textBox_CusID.Name = "textBox_CusID";
            this.textBox_CusID.Size = new System.Drawing.Size(380, 20);
            this.textBox_CusID.TabIndex = 15;
            // 
            // button_Clear
            // 
            this.button_Clear.Location = new System.Drawing.Point(60, 321);
            this.button_Clear.Name = "button_Clear";
            this.button_Clear.Size = new System.Drawing.Size(112, 36);
            this.button_Clear.TabIndex = 16;
            this.button_Clear.Text = "Clear";
            this.button_Clear.UseVisualStyleBackColor = true;
            this.button_Clear.Click += new System.EventHandler(this.button_Clear_Click);
            // 
            // label_Notofication
            // 
            this.label_Notofication.AutoSize = true;
            this.label_Notofication.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label_Notofication.ForeColor = System.Drawing.Color.Red;
            this.label_Notofication.Location = new System.Drawing.Point(352, 193);
            this.label_Notofication.Name = "label_Notofication";
            this.label_Notofication.Size = new System.Drawing.Size(68, 15);
            this.label_Notofication.TabIndex = 17;
            this.label_Notofication.Text = "Notification";
            // 
            // WinMDS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(874, 430);
            this.Controls.Add(this.label_Notofication);
            this.Controls.Add(this.button_Clear);
            this.Controls.Add(this.textBox_CusID);
            this.Controls.Add(this.label_CusID);
            this.Controls.Add(this.button_UpdateCustomer);
            this.Controls.Add(this.button_DeleteCustomer);
            this.Controls.Add(this.button_AddCustomer);
            this.Controls.Add(this.Mobile);
            this.Controls.Add(this.Email);
            this.Controls.Add(this.LastName);
            this.Controls.Add(this.FirstName);
            this.Controls.Add(this.textBox_mobile);
            this.Controls.Add(this.textBox_email);
            this.Controls.Add(this.textBox_lastName);
            this.Controls.Add(this.textBox_firstName);
            this.Controls.Add(this.buttonViewCustomer);
            this.Controls.Add(this.dataGridView1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "WinMDS";
            this.Text = "WinMDS";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button buttonViewCustomer;
        private System.Windows.Forms.TextBox textBox_firstName;
        private System.Windows.Forms.TextBox textBox_lastName;
        private System.Windows.Forms.TextBox textBox_email;
        private System.Windows.Forms.TextBox textBox_mobile;
        private System.Windows.Forms.Label FirstName;
        private System.Windows.Forms.Label LastName;
        private System.Windows.Forms.Label Email;
        private System.Windows.Forms.Label Mobile;
        private System.Windows.Forms.Button button_AddCustomer;
        private System.Windows.Forms.Button button_DeleteCustomer;
        private System.Windows.Forms.Button button_UpdateCustomer;
        private System.Windows.Forms.Label label_CusID;
        private System.Windows.Forms.TextBox textBox_CusID;
        private System.Windows.Forms.Button button_Clear;
        private System.Windows.Forms.Label label_Notofication;
    }
}

