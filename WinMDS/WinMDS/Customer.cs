﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinMDS
{
    public class Customer
    {
        public ObjectId Id { get; set; }

        public string cusID { get; set; }

        public string firstName { get; set; }

        public string lastName { get; set; }

        public string email { get; set; }

        public string mobile { get; set; }
    }
}
