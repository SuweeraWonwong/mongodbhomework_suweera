﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Bson.Serialization.Attributes;

namespace WinMDS
{
    public partial class WinMDS : Form
    {

        public WinMDS()
        {
            InitializeComponent();
            label_Notofication.Hide();
           
        }

        private void buttonViewCustomer_Click(object sender, EventArgs e)
        {
            label_Notofication.Hide();
            var connectionstring = "mongodb://localhost:27017";
            MongoClient client = new MongoClient(connectionstring);

            var db = client.GetDatabase("MyShop");
            var collection = db.GetCollection<Customer>("Customers");

            var data = collection.Find(_ => true).ToList();
            dataGridView1.DataSource = data;

        }

        private void button_AddCustomer_Click(object sender, EventArgs e)
        {
            
            var connectionstring = "mongodb://localhost:27017";
            MongoClient client = new MongoClient(connectionstring);

            var db = client.GetDatabase("MyShop");
            var collection = db.GetCollection<BsonDocument>("Customers");

            var cusID = textBox_CusID.Text;
            var firstname = textBox_firstName.Text;
            var lastname = textBox_lastName.Text;
            var email = textBox_email.Text;
            var mobile = textBox_mobile.Text;

            var document = new BsonDocument
            {
                {"cusID",cusID},
                {"firstName",firstname},
                {"lastName",lastname},
                {"email",email},
                {"mobile",mobile},
            };

            //Insert
            if (cusID != "" && firstname != "" && lastname != "")
            {
                collection.InsertOneAsync(document);

                label_Notofication.Show();
                label_Notofication.Text = "Completed";

            }
        }

        private async void button_DeleteCustomer_Click(object sender, EventArgs e)
        {
            var connectionstring = "mongodb://localhost:27017";
            MongoClient client = new MongoClient(connectionstring);

            var db = client.GetDatabase("MyShop");
            var collection = db.GetCollection<BsonDocument>("Customers");

            var cusID = textBox_CusID.Text;

            //Deleting 
            if (cusID != "")
            {
                var result = await collection.FindOneAndDeleteAsync(Builders<BsonDocument>.Filter.Eq("cusID", cusID));

                label_Notofication.Show();
                label_Notofication.Text = "Completed";

            }

        }

        private async void button_UpdateCustomer_Click(object sender, EventArgs e)
        {

            var connectionstring = "mongodb://localhost:27017";
            MongoClient client = new MongoClient(connectionstring);

            var db = client.GetDatabase("MyShop");
            var collection = db.GetCollection<BsonDocument>("Customers");

            var cusID = textBox_CusID.Text;
            var firstname = textBox_firstName.Text;
            var lastname = textBox_lastName.Text;
            var email = textBox_email.Text;
            var mobile = textBox_mobile.Text;

            //Update All
            if (cusID != "") {
                var result = await collection.UpdateOneAsync(Builders<BsonDocument>.Filter.Eq("cusID", cusID),
                       Builders<BsonDocument>.Update.Set("cusID", cusID).Set("firstName", firstname).Set("lastName", lastname).Set
                 ("email", email).Set("mobile", mobile));

                label_Notofication.Show();
                label_Notofication.Text = "Completed";
                
            }

        }

        private void button_Clear_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            textBox_CusID.Text = null;
            textBox_firstName.Text = null;
            textBox_lastName.Text = null;
            textBox_email.Text = null;
            textBox_mobile.Text = null;
            label_Notofication.Text = null;

        }

    }
}
